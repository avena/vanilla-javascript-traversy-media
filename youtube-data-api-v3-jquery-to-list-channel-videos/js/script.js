//var channelName = 'TechGuyWeb';
var channelId = 'UC7EqH71Q3e8mgOTIKeOExbw';

var vidWidth = '500';
var vidHeight = '400';
var vidResults = 10;


$(document).ready(function () {

  $.get(
    "https://www.googleapis.com/youtube/v3/channels", {
      part: 'contentDetails',
      id: channelId,
      //forUsername: channelName,
      key: 'AIzaSyDxDBGjO71SJrMr7jysYAG29LIS_JPWZx4'
    },

    function (data) {
      $.each(data.items, function (i, item) {
        // console.log(item);
        var pid = item.contentDetails.relatedPlaylists.uploads;

        getVids(pid);

      })
    }
  );

  function getVids(pid) {
    $.get(
      "https://www.googleapis.com/youtube/v3/playlistItems", {
        part: 'snippet',
        maxResults: vidResults,
        playlistId: pid,
        key: 'AIzaSyDxDBGjO71SJrMr7jysYAG29LIS_JPWZx4'
      },

      function (data) {
        // console.log(data);
        var output;
        $.each(data.items, function (i, item) {
          console.log(item);
          videoTitle = item.snippet.title;
          videoId = item.snippet.resourceId.videoId;
          thumbnailsMedium = item.snippet.thumbnails.medium;

          //output = `<li>${videoTitle}</li>`;
          output = `<li>
          <iframe height="${vidHeight}" width="${vidWidth}" src="http://www.youtube.com/embed/${videoId}"></iframe>
          </li>`;

          $('#results').append(output);

        })
      }
    );
  }


});

