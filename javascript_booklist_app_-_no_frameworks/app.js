// Classe do Livro: Representando o Livro
class Book {
  constructor(title, author, isbn) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
  }
}

// UI Class: Lida com Tarefas da Interface
class UI {
  static displayBooks() {
    const books = Store.getBooks();

    books.forEach((book) => UI.addBookToList(book));
  }

  static addBookToList(book) {
    const list = document.querySelector('#book-list');

    const row = document.createElement('tr');

    row.innerHTML = `
      <td>${book.title}</td>
      <td>${book.author}</td>
      <td>${book.isbn}</td>
      <td><a href="#" class="btn btn-danger btn-sm delete">X</td>
    `

    list.appendChild(row);
  }

  static deleteBook(el) {
    if (el.classList.contains('delete')) {
      // remove o parente (aparentado) do parente (aparentado)
      el.parentElement.parentElement.remove();
    }
  }


  static showAlert(msg, className) {
    const div = document.createElement('div');
    div.className = `alert alert-${className}`;
    div.appendChild(document.createTextNode(msg));
    const container = document.querySelector('.container');
    const form = document.querySelector('#book-form');
    container.insertBefore(div, form);
    // remover alerta depois de 3 segundos
    setTimeout(() => document.querySelector('.alert').remove(), 3000);

  }

  static clearFields() {
    document.querySelector('#title').value = '';
    document.querySelector('#author').value = '';
    document.querySelector('#isbn').value = '';
  }

}


// Store Class: Lida com o armazenamento
class Store {
  static getBooks() {
    // não se guarda objetos, se guarda strings
    let books;
    if (localStorage.getItem('books') === null) {
      books = [];
    } else {
      // transformando de string para objeto
      books = JSON.parse(localStorage.getItem('books'));
    }

    return books;
  }

  static addBook(book) {
    const books = Store.getBooks();

    books.push(book);
    // transformando de objeto para string (que vai ser armazenado)
    localStorage.setItem('books', JSON.stringify(books));
  }

  static removeBook(isbn) {
    const books = Store.getBooks();

    books.forEach((book, index) => {
      if (book.isbn === isbn) {
        books.splice(index, 1);
      }

    });

    localStorage.setItem('books', JSON.stringify(books));

  }

}

// Event: Mostra os livros
document.addEventListener('DOMContentLoaded', UI.displayBooks);


// Event: Adicionar um livro
document.querySelector('#book-form').addEventListener('submit', (e) => {

  // prevenindo o submit envio
  e.preventDefault();

  // Pegando valores do formulario
  const title = document.querySelector('#title').value;
  const author = document.querySelector('#author').value;
  const isbn = document.querySelector('#isbn').value;

  // Validar
  if (title === '' || author === '' || isbn === '') {
    // alert('Por favor preencha todos os campos');
    UI.showAlert('Por favor preencha todos os campos', 'danger');
  } else {

    // Instanciando um livro
    const book = new Book(title, author, isbn);

    // console.log(book);
    // Adicionar livro a interface
    UI.addBookToList(book);

    // Adicionar livro ao localStorage
    Store.addBook(book);

    // Mostrar mensagem de sucesso de cadastro
    UI.showAlert('Livro Adicionado', 'success');


    // limpar os campos
    UI.clearFields();

  }

})


// Event: Remover os livros - 25 Minutos
// https://youtu.be/JaMCxVWtW58?t=1518
// para pegar cada item/livro, na verdade usa o backpropagation
document.querySelector('#book-list').addEventListener('click', (e) => {
  //console.log(e.target);
  // remove o livro da interface
  UI.deleteBook(e.target);

  // remove o livro do localStorage
  Store.removeBook(e.target.parentElement.previousElementSibling.textContent);

  // mostrar mensagem de livro removido
  UI.showAlert('Livro Removido', 'success');

});