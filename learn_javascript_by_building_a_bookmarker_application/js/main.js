// listen for forms submit
document.getElementById('myForm').addEventListener('submit', saveBookmark);

// save Bookmark
function saveBookmark(e){
  // get form values
  var siteName = document.getElementById('siteName').value;
  var siteUrl = document.getElementById('siteUrl').value;

  if(!validateForm(siteName,siteUrl)) {
    return false;
  }

  // criando o objeto bookmark
  var bookmark = {
    name: siteName,
    url: siteUrl
  }

  // local storage test
  // armazena somente string, entrao objeto json para string e depois de string para objeto
  // configura item - localStorage.setItem()
  // pega item - localStorage.getItem()
  //  remove item - localStorage.removeItem()
  /*
  localStorage.setItem('teste', 'ola mundao');
  console.log(localStorage.getItem('teste'));

  localStorage.removeItem('teste');
  console.log(localStorage.getItem('teste'));
  */
  // teste if bookmarks is null
  if(localStorage.getItem('bookmarks') === null) {
    // init array
    var bookmarks = [];
    // add to array
    bookmarks.push(bookmark);
    // set to localStorage - json to string
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
  } else {
    // get bookmasks from localStorage - string to json
    var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    // add to array
    bookmarks.push(bookmark);
    // re-set to bakc to localhost -  json to string
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
  }

  // clear forms
  document.getElementById('myForm').reset();


  // re-fetch bookmark
  fetchBookmarks();

  // prevent form from submitting
  e.preventDefault();
}

// delete bookmark
function deleteBookmark(url) {
  // get bookmasks from localStorage - string to json
  var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));

  //loop pelos bookmarks
  for(var i = 0; i < bookmarks.length; i++) {
    if(bookmarks[i].url == url) {
      // remove from array
      bookmarks.splice(i,1);
    }
  }
  // re-set to bakc to localhost -  json to string
  localStorage.setItem('bookmarks', JSON.stringify(bookmarks));

  // re-fetch bookmark
  fetchBookmarks();
}

// fetch bookmasks
function fetchBookmarks() {
  // get bookmasks from localStorage - string to json
  var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));

  // get output id
  var bookmarksResults = document.getElementById('bookmarksResults');

  // build output
  bookmarksResults.innerHTML = '';
  for(var i = 0; i < bookmarks.length; i++) {
    var name = bookmarks[i].name;
    var url = bookmarks[i].url;

    bookmarksResults.innerHTML += '<div class="well">' +
                                  '<h3>' + name +
                                  ' <a class="btn btn-default" target="_blank" href="' + url +'">Visite</a>' +
                                  ' <a onclick="deleteBookmark(\''+url+'\')" class="btn btn-danger" href="#">Remover</a>' +
                                  '</h3>' +
                                  '</div>';

  }

}


function validateForm(siteName, siteUrl) {
  // validacao - se nada for preenchido
  if(!siteName || !siteUrl) {
    alert('Preencha o formulario');
    return false;
  }

  // verificando um url valida
  var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
  var regex = new RegExp(expression);
  if(!siteUrl.match(regex)) {
    alert("Use uma url valida");
    return false;
  }

  return true;

}
